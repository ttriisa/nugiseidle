import { createStore, applyMiddleware, compose } from 'redux';
import rootReducer from "../redux/reducers"
import thunk from "redux-thunk";

const composeEnhancers =
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
            // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
        }) : compose;

const middleware = [
    thunk
];

function configureStore(initialState) {
    return createStore(rootReducer, initialState, composeEnhancers(
        applyMiddleware(...middleware),
        // other store enhancers if any
    ));
}

export const store = configureStore();

export const mapDispatcher = (props, wrapWithObject=true) => {
    return (dispatch/*, ownProps */) => {
        const actions = {};
        for(let key in props) {
            if(props.hasOwnProperty(key))
                actions[key] = (...params) => dispatch(props[key](...params));
        }
        actions.dispatch = dispatch;
        if(wrapWithObject)
            return {actions};
        return actions;
    }
}