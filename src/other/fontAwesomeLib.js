import { library } from '@fortawesome/fontawesome-svg-core';

import { faAxe, faShovel, faTrees, faTreeAlt, faSeedling, faHorseSaddle, faBallPile, faPie, faFireSmoke, faPumpkin, faCauldron,
    faKnifeKitchen, faCircle, faHammer, faTools } from '@fortawesome/pro-duotone-svg-icons';
//import { faCode, faHighlighter } from '@fortawesome/free-solid-svg-icons';

library.add(
    faAxe,
    faShovel,
    faTreeAlt,
    faTrees,
    faSeedling,
    faHorseSaddle,
    faBallPile,
    faPie,
    faFireSmoke,
    faPumpkin,
    faCauldron,
    faKnifeKitchen,
    faCircle,
    faHammer,
    faTools,
);