import React, { useState, useEffect, useRef } from 'react';

export function useInterval(callback, delay) {
  const savedCallback = useRef();
  const intervalId = useRef(null);

  // Remember the latest callback.
  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  // Set up the interval.
  useEffect(() => {
    function tick() {
      savedCallback.current();
    }
    if (delay !== null) {
      console.log("useInterval useEffect restart interval", intervalId.current);
      intervalId.current = setInterval(tick, delay);
      return () => {
        console.log("useInterval clearTimeout", intervalId.current);
        clearInterval(intervalId.current);
        intervalId.current = null;
      };
    }
  }, [delay]);
}

export function useTimeout(callback, delay) {
  const savedCallback = useRef();
  const intervalId = useRef(null);

  // Remember the latest callback.
  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  // Set up the interval.
  useEffect(() => {
    console.log("useTimeout", intervalId.current, delay);
    function tick() {
      console.log("useTimeout tick");
      savedCallback.current();
    }
    if (delay !== null && intervalId.current === null) {
      intervalId.current = setTimeout(tick, delay);
      return () => {
        console.log("useTimeout clearTimeout", intervalId.current);
        clearTimeout(intervalId.current);
        intervalId.current = null;
      };
    }
  }, []);
}