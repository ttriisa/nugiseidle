import { useState } from "react"

/**
 * LocalStorage writes are delayed up to 10 seconds to reduce SSD wear
 * @param name
 * @param [defaultValue]
 * @return {*[]}
 */
export default function useLocalStorage(name, defaultValue = {}) {
    let timerId, lastCheck = (new Date).getTime(), lastCall = 0;
    let data = localStorage.getItem(name);
    if(data === null)
        data = defaultValue
    else
        data = JSON.parse(data)
    return [
        data,
        (value, delay = 10000) => {
            const now = (new Date).getTime();
            if (!timerId)
                lastCall = now;
            if (now - lastCheck < delay)
                clearTimeout(timerId);
            timerId = setTimeout(() => {
                try {
                    localStorage.setItem(name, JSON.stringify(value));
                    console.log("localStorage has been updated.")
                } catch (e) {
                    // localStorage is not available
                }
                timerId = null;
                lastCall = (new Date).getTime();
            }, (delay - (now - lastCall)) || 0);
            lastCheck = now;
        }
    ];
}
//window.useLocalStorage = useLocalStorage;

/**
 * State is set immediately, but localStorage writes are delayed up to 10 seconds to reduce SSD wear
 * @param name
 * @param [defaultValue]
 * @return {*[]}
 */
export function useLocalStorageState(name, defaultValue = {}) {
    let timerId, lastCheck = (new Date).getTime(), lastCall = 0;
    let data = localStorage.getItem(name);
    if(data === null)
        data = defaultValue
    else
        data = JSON.parse(data)
    const [value, setValue] = useState(data);
    return [
        value,
        (value, delay = 10000) => {
            setValue(value);
            const now = (new Date).getTime();
            if (!timerId)
                lastCall = now;
            if (now - lastCheck < delay)
                clearTimeout(timerId);
            timerId = setTimeout(() => {
                try {
                    localStorage.setItem(name, JSON.stringify(value));
                } catch (e) {
                    // localStorage is not available
                }
                timerId = null;
                lastCall = (new Date).getTime();
            }, (delay - (now - lastCall)) || 0);
            lastCheck = now;
        }
    ];
}