export const ActionTypes = {
    increaseResource: "increaseResource",
    increaseCounter: "increaseCounter",
    enableBuilding: "enableBuilding",

    dig: "dig",
    fillHole: "fillHole",
    pileWood: "pileWood",
    clearWoodPile: "clearWoodPile",

    changePlayerName: "changePlayerName",
    toggleTheme: "toggleTheme"
}