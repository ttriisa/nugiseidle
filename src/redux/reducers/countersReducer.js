import produce from 'immer';
import {ActionTypes} from "../actionTypes"
import useLocalStorage from "../../other/localStorage";

const [initialState, saveState] = useLocalStorage("counters", {
    holeDepth: 0,
    woodPile: 0,
});

export default function countersReducer(state = initialState, action) {
    let newState = state;
    switch (action.type) {
        case ActionTypes.increaseCounter:
            newState = produce(state, draftState => {
                draftState[action.name] = state[action.name] + action.amount;
            });
            saveState(newState, 30000);
            return newState;
        case ActionTypes.dig:
            newState = produce(state, draftState => {
                draftState.holeDepth = state.holeDepth + action.amount;
            });
            saveState(newState, 30000);
            return newState;
        case ActionTypes.fillHole:
            newState = produce(state, draftState => {
                draftState.holeDepth = 0;
            });
            saveState(newState, 30000);
            return newState;
        case ActionTypes.pileWood:
            newState = produce(state, draftState => {
                draftState.woodPile = state.woodPile + action.amount;
            });
            saveState(newState, 30000);
            return newState;
        case ActionTypes.clearWoodPile:
            newState = produce(state, draftState => {
                draftState.woodPile = 0;
            });
            saveState(newState, 30000);
            return newState;
        default:
            return newState;
    }
}