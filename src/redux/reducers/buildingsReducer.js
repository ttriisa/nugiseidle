import produce from 'immer';
import {ActionTypes} from "../actionTypes"
import useLocalStorage from "../../other/localStorage";

const [initialState, saveState] = useLocalStorage("buildings", {
    hole: false,
    charcoal: false,
    workbench: false,
    well: false,
    horse: false,
    trader: false,
});

export default function buildingsReducer(state = initialState, action) {
    let newState = state;
    switch (action.type) {
        case ActionTypes.enableBuilding:
            newState = produce(state, draftState => {
                draftState[action.name] = action.enabled;
            });
            saveState(newState, 30000);
            return newState;
        default:
            return newState;
    }
}