import produce from 'immer';
import {ActionTypes} from "../actionTypes"
import useLocalStorage from "../../other/localStorage";

const [initialState, saveState] = useLocalStorage("resources", {
    food: 1200,
    water: 2000,
    //dirt: null,
    //sand: null,
    //clay: null,
//
    //wood: null,
    //charcoal: null,
//
    //limestone: null,
    //granite: null,
    //dolomite: null,
    //sulfur: null,
//
    //glass: null,
    //ash: null,
//
    //magnesium: null, //dolomite
    //silica: null,
});

export default function resourcesReducer(state = initialState, action) {
    let newState = state;
    switch (action.type) {
        case ActionTypes.increaseResource:
            let amount = state[action.name];
            newState = produce(state, draftState => {
                draftState[action.name] = (amount === undefined ? 0 : amount) + action.amount;
            });
            saveState(newState, 30000);
            return newState;
        case ActionTypes.fillHole:
            newState = produce(state, draftState => {
                draftState.dirt = state.dirt - 10;
            });
            saveState(newState, 30000);
            return newState;
        case ActionTypes.pileWood:
            newState = produce(state, draftState => {
                draftState.wood = state.wood - action.amount;
            });
            saveState(newState, 30000);
            return newState;
        default:
            return newState;
    }
}