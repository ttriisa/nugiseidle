import { combineReducers } from 'redux';
import resources from "./resourcesReducer";
import buildings from "./buildingsReducer";
import counters from "./countersReducer";
import general from "./generalReducer";

const rootReducer = combineReducers({
    resources,
    buildings,
    counters,
    general,
});

export default rootReducer;