import produce from 'immer';
import {ActionTypes} from "../actionTypes"
import {getRandomBiome} from "../../components/biome";
import generateName from "../../other/helpers";
import useLocalStorage from "../../other/localStorage";

export const themeLight = {
    backgroundColor: "inherit",
    color: "inherit"
};
export const themeDark = {
    backgroundColor: "#161616",
    color: "#EBF4F4"
};

const [initialState, saveState] = useLocalStorage("general", {
    playerName: generateName() || "Untitled",
    biome: getRandomBiome(),
    theme: themeLight
});

export default function generalReducer(state = initialState, action) {
    let newState = state;
    switch (action.type) {
        case ActionTypes.changePlayerName:
            newState = produce(state, draftState => {
                draftState.playerName = action.playerName;
            });
            saveState(newState, 0);
            return newState;
        case ActionTypes.toggleTheme:
            newState = produce(state, draftState => {
                draftState.theme = state.theme === themeLight ? themeDark : themeLight;
            });
            saveState(newState, 0);
            return newState;
        default:
            return newState;
    }
}