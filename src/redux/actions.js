import { ActionTypes } from "./actionTypes";

export function increaseResource(name, amount) {
    return { type: ActionTypes.increaseResource, name, amount }
}

export function increaseCounter(name, amount) {
    return { type: ActionTypes.increaseCounter, name, amount }
}

export function enableBuilding(name, enabled=true) {
    return { type: ActionTypes.enableBuilding, name, enabled }
}

export function fillHole() {
    return { type: ActionTypes.fillHole }
}

export function pileWood(amount) {
    return { type: ActionTypes.pileWood, amount }
}

export function clearWoodPile() {
    return { type: ActionTypes.clearWoodPile }
}

export function setPlayerName(playerName) {
    return { type: ActionTypes.changePlayerName, playerName }
}

export function toggleTheme() {
    return { type: ActionTypes.toggleTheme }
}

export function dig(amount) {
    return { type: ActionTypes.dig, amount }
}