import React, { useEffect } from "react";
import { connect } from "react-redux";
import { increaseResource, enableBuilding } from "../redux/actions";
import { ASCII } from "../other/asciiart";
import withBuilding from "./Building";
import { Craft } from "./Buttons";

function Workbench({isBuilt, wood, dispatch}) {
    const workbenchCost = 15;
    const craftAxe = () => {};
    const craftKnife = () => {};
    const craftHammer = () => {};
    const craftTongs = () => {};
    const craftPickaxe = () => {};

    const buildWorkbench = () => {
        dispatch(increaseResource("wood", -workbenchCost));
        dispatch(enableBuilding("workbench"));
    }

    if(!isBuilt) {
        if(wood >= workbenchCost)
            return <>
            <h2>&nbsp;</h2>
            <ASCII.Pre/>
            <Craft icon={['fad', 'hammer']} text="Build a workbench" material={{wood: workbenchCost}} time={20} onCooldown={buildWorkbench} /></>
        return null;
    }

    return <>
        <h2>Workbench</h2>
        {ASCII.Workbench}
        <button onClick={craftAxe}>Craft a stone axe</button><br/>
        <button onClick={craftKnife}>Craft a obsidian knife</button><br/>
        <button onClick={craftHammer}>Craft a stone hammer</button><br/>
        <button onClick={craftTongs}>Craft a wooden tongs</button><br/>
        <button onClick={craftPickaxe}>Craft a pickaxe</button><br/>
    </>;
}
const mapStateToProps = (store /*, ownProps*/) => {
    return {
        isBuilt: store.buildings.workbench,
        wood: store.resources.wood,
    };
}
export default withBuilding(connect(mapStateToProps)(Workbench))