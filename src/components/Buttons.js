import React, { useState, useEffect, useRef } from "react";
import styled from "styled-components";
import {useInterval, useTimeout} from "../other/useInterval";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { capFirst, addSpaces } from "../other/helpers";
import { connect } from "react-redux";
import { increaseResource } from "../redux/actions";

export const WideBtn = styled.button`
    display: block;
    text-align: left;
    min-width: 10rem;
    padding-inline-start: 0.5rem;
    padding-inline-end: 0.5rem;
    padding-block-start: 0.3rem;
    padding-block-end: 0.3rem;
    svg {
        padding-inline-end: 0.3rem;
    }
`;

export function TimedBtn({show=true, onClick, onCooldown, time=1, text, icon, children, disabled,
    ...props}) {
    const [, updateTime] = useState({});
    const [isCooldown, setCooldown] = useState(false);
    const [endTime, setEndTime] = useState(0);
    const intervalDelay = 100;
    const timeoutId = useRef(null);
    const intervalId = useRef(null);
    //let timeoutId = null;
    //let intervalId = null;
    
    const handleCooldown = () => {
        setEndTime(0);
        setCooldown(false);
        if(typeof onCooldown === "function") {
            onCooldown();
        }
    }

    const handleClick = () => {
        if(!isCooldown) {
            if(typeof onClick === "function") {   
                onClick();
            }
            setEndTime(Date.now() + parseFloat(time) * 1000);
            setCooldown(true);
        }
    }

    const updateTimeLeft = () => {
        updateTime({});
    }

    useEffect(() => {
        if(isCooldown) {
            clearTimeout(timeoutId.current);
            clearInterval(intervalId.current);
            timeoutId.current = setTimeout(handleCooldown, Math.max(0, endTime - Date.now()));
            intervalId.current = setInterval(updateTimeLeft, intervalDelay);
        }
        return () => {
            //clearTimeout(timeoutId.current);
            clearInterval(intervalId.current);
        }
    }, []);

    useEffect(() => {
        let timeLeft = endTime - Date.now();
        if(timeLeft > 0) {
            clearTimeout(timeoutId.current);
            clearInterval(intervalId.current);
            timeoutId.current = setTimeout(handleCooldown, Math.max(0, timeLeft));
            intervalId.current = setInterval(updateTimeLeft, intervalDelay);
        }
    }, [endTime]);

    if(!show && !isCooldown)
        return null;
    return <WideBtn disabled={isCooldown || disabled} onClick={handleClick} {...props}>
        {icon && <FontAwesomeIcon icon={icon}/>}{text || children}
        {isCooldown && ` (${((endTime - Date.now())/1000).toFixed(1)})`}
    </WideBtn>;
}

export function IconBtn({show=true, icon, text, children, ...props}) {
    if(!show)
        return null;
    return <WideBtn {...props}>
        {icon && <FontAwesomeIcon icon={icon}/>}{text || children}
    </WideBtn>;
}

export function CraftInternal({onClick, resources, material, text, children, disabled, show="after", dispatch, ...props}) {

    const handleClick = () => {
        // Use up resources before crafting starts
        for (const materialName in material) {
            dispatch(increaseResource(materialName, -material[materialName]));
        }

        if(typeof onClick === "function") {   
            onClick();
        }
    }
    
    // Check if player has enough resources
    let hasResources = true;
    for (const materialName in material) {
        const amountNeeded = material[materialName];
        const amountAvailable = resources[materialName] || 0;
        if(amountAvailable < amountNeeded) {
            hasResources = false;
            break;
        }
    }

    // By default show only when enough resources
    if(show === "after") {
        show = hasResources;
    }

    return <TimedBtn onClick={handleClick} disabled={!hasResources || disabled} show={show} {...props}>
        <Needs text={text || children} material={material}/>
    </TimedBtn>
}
function mapStateToProps(store) {
    return {resources: store.resources};
}
export const Craft = connect(mapStateToProps)(CraftInternal);

const ResourceList = styled.ul`
    display: block;
    font-size: 0.7em;
    font-style: italic;
    padding-inline-start: 1.9rem;
    margin: 0.1rem;
`;

export function Needs({text, material, children, ...props}) {
    return <>
        <span>{text || children}</span>
        <ResourceList>
            {Object.entries(material).map(res => {
                return <li key={res[0]}>{capFirst(addSpaces(res[0]))+": "+res[1]}</li>
                //return <li key={res[0]}>{res[1]+"x "}{addSpaces(res[0])}</li>
            })}
        </ResourceList>
    </>;
}