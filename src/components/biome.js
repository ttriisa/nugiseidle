export const biomes = {
    forest: "forest", // wood, berries
    beach: "beach", // salt water
    plain: "plain", // soil, (dig hole, dig deeper, dig deeper, water, build well)
    volcano: "volcano", // dolomite(zinc, cobalt, magnesium), sulfur, saltpeter, (very little by hand, most with explosive): raw diamond, quartz and obsidian 
    mountain: "mountain", // granite, (nickel, lead, zinc, copper)
    valley: "desert valley", // clay, limestone, gold, silver
    crater: "crater lake", // magnetite, iron, quicksilver
    swamp: "swamp", // iron(0.5), peat(1), water(1), clay(0.2)
    default: "default",
}
export function getRandomBiome() {
    // network request
    let availableBiomes = Object.keys(biomes);
    return availableBiomes[Math.floor(Math.random()*availableBiomes.length-1)]; // ignore the last one
}

export const biomeResourceMap = {
    [biomes.field]: {
        dig: {
            dirt: {chance: 1, amount: 1},
            clay: {chance: 0.2, amount: [0.1,0.5]},
            wheatSeed: {chance: 0.05, amount: 1},
            grass: {chance: 0.1, amount: 1},
        },
        cutGrass: {
            grass: {change:1, amount: [1,2,0]},
        },
        chopWood: {
            wood: {chance: 1, amount: [0.5,1]}
        },
        searchBushes: {
            food: {change: 0.2, amount: [0.2,2.2]},
            horses: {change: 0.01, amount: 1}
        }
    },
    [biomes.forest]: {
        dig: {
            dirt: {chance: 1, amount: 1},
            clay: {chance: 0.2, amount: [0.1,0.5]},
            wheatSeed: {chance: 0.05, amount: 1},
            grass: {chance: 0.05, amount: 1},
        },
        chopWood: {
            wood: {chance: 1, amount: [3,5]}
        }
    },
    [biomes.default]: {
        dig: {
            dirt: {chance: 1, amount: 1},
            clay: {chance: 0.2, amount: [0.1,0.5]},
            wheatSeed: {chance: 0.05, amount: 1},
            grass: {chance: 0.1, amount: 1},
        },
        cutGrass: {
            grass: {change:1, amount: [1,2,0]},
        },
        chopWood: {
            wood: {chance: 1, amount: [0.5,1]}
        },
        searchBushes: {
            food: {change: 0.2, amount: [0.2,2.2]}
        }
    }
}