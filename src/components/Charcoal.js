import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { enableBuilding, increaseResource, pileWood, clearWoodPile } from "../redux/actions";
import { ASCII } from "../other/asciiart";
import { TimedBtn, Needs, Craft } from "./Buttons";
import withBuilding from "./Building";

function Charcoal({isBuilt, resources, woodPile, dispatch}) {
    const [onFire, setOnFire] = useState(false);
    const [inUse, setUsage] = useState(false);

    let dirtNeeded = 15;
    if(resources.dirt >= 15 && woodPile > 5) {
        dirtNeeded = 13 + woodPile * 0.5;
    }

    const setInUse = () => {
        console.log("set in use");
        setUsage(true);
    }

    const coverWithDirt = (type) => {
        dispatch(enableBuilding("charcoal"));
        setUsage(false);
    }

    const setItOnFire = () => {
        let charcoalMultiplier = 1, ashMultiplier = 0.5, dirtMultiplier = 0;
        if(isBuilt) {
            charcoalMultiplier = 4;
            ashMultiplier = 0.2;
            dirtMultiplier = 0.2;
        }
        setOnFire(setTimeout(() => {
            dispatch(enableBuilding("charcoal", false));
            dispatch(increaseResource("charcoal", woodPile * 0.2 * charcoalMultiplier));
            dispatch(increaseResource("ash", woodPile * ashMultiplier));
            dispatch(increaseResource("dirt", woodPile * dirtMultiplier));
            dispatch(clearWoodPile());
            setOnFire(false);
            // TODO add log
        }, 14000));
    }

    const handleAddWood = () => dispatch(pileWood(5));
    const handleAddWoodDuringFire = () => {
        clearTimeout(onFire);
        dispatch(pileWood(5));
        setItOnFire();
    }

    if(!isBuilt) {
        if(onFire) {
            return <>
                <h2>Campfire</h2>
                {ASCII.Campfire}
                <TimedBtn disabled={resources.wood < 5} icon={['fad', 'ball-pile']} text="Add 5 wood to the fire" time={0.1} onCooldown={handleAddWoodDuringFire} />
            </>
        }
        if(woodPile > 0) {
            return <div>
                <h2>Pile of wood</h2>
                {ASCII.PileOfWood}
                <div>Wood in pile: {woodPile}</div>
                <TimedBtn disabled={resources.wood < 5 || inUse} icon={['fad', 'ball-pile']} text="Add 5 wood to the pile" time={0.1} onCooldown={handleAddWood} />
                <TimedBtn show={!onFire} disabled={inUse} icon={['fad', 'fire-smoke']} text="Set it on fire" onCooldown={setItOnFire} />
                <Craft show={resources.dirt >= 15} icon={['fad', 'shovel']} text="Cover with dirt" material={{dirt: dirtNeeded}} time={8} onCooldown={coverWithDirt} onClick={setInUse} />
            </div>;
        }
        return <>
            <h2>&nbsp;</h2>
            <ASCII.Pre/>
            <TimedBtn show={resources.wood >= 5} icon={['fad', 'ball-pile']} text="Make a pile of wood" onCooldown={handleAddWood} />
        </>;
    }

    return <>
        <h2>Charcoal hill</h2>
        {onFire ? ASCII.CharcoalHillOnFire : ASCII.CharcoalHill}
        <TimedBtn show={!onFire} icon={['fad', 'fire-smoke']} text="Set it on fire" onCooldown={setItOnFire} />
    </>
}
const mapStateToProps = (store /*, ownProps*/) => {
    return {
        isBuilt: store.buildings.charcoal,
        holeDepth: store.counters.holeDepth,
        resources: {
            dirt: store.resources.dirt,
            wood: store.resources.wood,
        },
        woodPile: store.counters.woodPile,
    };
}
export default withBuilding(connect(mapStateToProps)(Charcoal))