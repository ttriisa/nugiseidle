import React from "React";
import styled from "styled-components";

const Building = styled.div`
    display: inline-block;
    &:not(:empty) {
        margin-inline-start: 0.5rem;
        margin-inline-end: 0.5rem;
    }
`;

export default function withBuilding(WrappedComponent) {
    return class extends React.Component {
        constructor(props) {
            super(props);
            //this.handleChange = this.handleChange.bind(this);
            //this.state = {
            //    data: selectData(DataSource, props)
            //};
        }

        //componentDidMount() {
        //  // ... that takes care of the subscription...
        //  DataSource.addChangeListener(this.handleChange);
        //}

        //componentWillUnmount() {
        //  DataSource.removeChangeListener(this.handleChange);
        //}

        //handleChange() {
        //  this.setState({
        //      data: selectData(DataSource, this.props)
        //  });
        //}

        render() {
            return <Building><WrappedComponent {...this.props} /></Building>;
        }
    };
}