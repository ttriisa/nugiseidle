import React, { useEffect } from "react";
import { connect } from "react-redux";
import { enableBuilding, fillHole, increaseResource } from "../redux/actions";
import { ASCII } from "../other/asciiart";
import { TimedBtn } from "./Buttons";
import withBuilding from "./Building";

function Mill({isBuilt, holeDepth, material, dispatch}) {
    const smallHoleDepth = 3;

    useEffect(() => {
        if(!isBuilt && holeDepth >= smallHoleDepth) {
            dispatch(enableBuilding("hole"));
        }
    }, [holeDepth]);

    const handleFillHole = (type) => {
        if(material.dirt >= smallHoleDepth) {
            dispatch(fillHole());
            dispatch(enableBuilding("hole", false));
        }
    }

    const plantSeed = (type) => {
        dispatch(fillHole());
        dispatch(increaseResource("wheatSeed", -1));
        dispatch(enableBuilding("hole", false));
        dispatch(enableBuilding("farmland", true));
    }

    if(!isBuilt)
        return null;

    return <>
        <h2>Hole</h2>
        {ASCII.EmptyHole}
        {material.dirt >= smallHoleDepth && <TimedBtn icon={['fad', 'shovel']} onCooldown={handleFillHole.bind(null, "dirt")}>Fill a hole with dirt</TimedBtn>}
        {material.sand >= smallHoleDepth && <TimedBtn icon={['fad', 'shovel']} onCooldown={handleFillHole.bind(null, "sand")}>Fill a hole with sand</TimedBtn>}
        {material.wheatSeed && material.dirt >= smallHoleDepth && <TimedBtn icon={['fad', 'seedling']} onCooldown={plantSeed.bind(null, "wheatSeed")}>Plant a wheat seed</TimedBtn>}
    </>
}
const mapStateToProps = (store /*, ownProps*/) => {
    return {
        isBuilt: store.buildings.hole,
        holeDepth: store.counters.holeDepth,
        material: {
            dirt: store.resources.dirt,
            sand: store.resources.sand,
            wheatSeed: store.resources.wheatSeed,
        }
    };
}
export default withBuilding(connect(mapStateToProps)(Hole))