import React, { useEffect } from "react";
import { connect } from "react-redux";
import { enableBuilding, fillHole, increaseResource } from "../redux/actions";
import { ASCII } from "../other/asciiart";
import { TimedBtn } from "./Buttons";
import withBuilding from "./Building";

function Trading({isBuilt, holeDepth, material, dispatch}) {
    const smallHoleDepth = 3;

    useEffect(() => {
        if(!isBuilt && holeDepth >= smallHoleDepth) {
            dispatch(enableBuilding("hole"));
        }
    }, [holeDepth]);

    const handleFillHole = (type) => {
        if(material.dirt >= smallHoleDepth) {
            dispatch(fillHole());
            dispatch(enableBuilding("hole", false));
        }
    }

    const plantSeed = (type) => {
        dispatch(fillHole());
        dispatch(increaseResource("wheatSeed", -1));
        dispatch(enableBuilding("hole", false));
        dispatch(enableBuilding("farmland", true));
    }

    if(!isBuilt)
        return null;

    return <>
        <h2>Trading</h2>
        <ASCII.Horse/>
    </>
}
const mapStateToProps = (store /*, ownProps*/) => {
    return {
        isBuilt: store.buildings.trading,
        material: {
            dirt: store.resources.dirt,
            sand: store.resources.sand,
            wheatSeed: store.resources.wheatSeed,
        }
    };
}
export default withBuilding(connect(mapStateToProps)(Trading))