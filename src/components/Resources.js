import React, { useState, useEffect, useRef } from "react";
import { capFirst, addSpaces, usePrevious } from "../other/helpers";
import { ASCII } from "../other/asciiart";
import { connect } from "react-redux";
import styled, { keyframes } from "styled-components";
import withBuilding from "./Building";

const Th = styled.th`
    text-align: right;
    padding-inline-end: 0.3rem;
`;

const Diff = styled.span`
    padding-inline-start: 0.2rem;
    color: ${props => props.color};
    visibility: ${props => props.show ? 'visible' : 'hidden'};
    animation: ${props => props.show && fadeOut} 1.5s ease-in;
`;

const fadeOut = keyframes`
  from {
    opacity: 1;
  }

  to {
    opacity: 0;
  }
`;

function Resources({resources}) {
    return <>
        <h2>Resources</h2>
        {ASCII.Chest}
        <table>
            <tbody>
                {Object.entries(resources).map(([name, value]) => <Resource key={name} label={capFirst(addSpaces(name))} value={value} />)}
            </tbody>
        </table>
    </>;
}
function mapStateToProps(store) {
    return {resources: store.resources};
}
export default withBuilding(connect(mapStateToProps)(Resources));

function Resource({label, value}) {
    const prevValue = usePrevious(value);
    const [showDiff, setShowDiff] = useState(0);
    const timeoutId = useRef();

    useEffect(() => {
        if(prevValue !== value && !isNaN(value-prevValue)) {
            setShowDiff(value-prevValue);
            if(timeoutId.current)
                clearTimeout(timeoutId.current);
            timeoutId.current = setTimeout(() => setShowDiff(0), 1500);
        }
    }, [value]);

    if(value === null)
        return null;
    return <tr>
        <Th>{label}</Th>
        <td>{parseFloat(value.toFixed(2))}<Diff show={showDiff !== 0} color={showDiff < 0 ? 'red' : 'green'}>({(showDiff<0?"":"+")+showDiff.toFixed(1)})</Diff></td>
    </tr>;
}

