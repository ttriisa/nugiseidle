import React, { Component, useState, useEffect } from "react";
import ReactDOM from "react-dom";
import Resources from "./components/Resources";
import {biomes, biomeResourceMap} from "./components/biome";
import { ASCII } from "./other/asciiart";
import { store, mapDispatcher } from "./other/reduxConfig";
import { enableBuilding, increaseResource, setPlayerName, increaseCounter, toggleTheme } from "./redux/actions";
import { Provider, connect } from "react-redux";
import Hole from "./components/Hole";
import Workbench from "./components/Workbench";
import Charcoal from "./components/Charcoal";
import { getRandomPrecision } from "./other/helpers";
import { TimedBtn } from "./components/Buttons";
import "./other/fontAwesomeLib";
import withBuilding from "./components/Building";

// function calculateResourceAmount() uses biome and equipped tools
// et toitu ja vett läheks maha
// hobused ja hobused tarbivad vett, trading ühe hobusega
// ehita tall, et hobuseid breedida?
// ehita vanker et rohkem kaupa vedada
// grass -> string -> rope
// knife -> gather wood bark -> rope
// clay bucket, wooden bucket (bucket on ainult üks resource aga materjale)
// vee keetmine, salt
// metallide sulatamine
// swamp biome sealt tuleb iron ore
// väetised: turvas kaltsium
// build a cart mk1 -> wagon mk2
// send a trading cart to player
// kilns (tar (wood), quicklime (limestone), maybe just chemical salts)
// metal tools
// alchemy workbench (needs glass) and vials etc uses chemical salts to make acids etc
//    insulate wire with resin (how to collect resin?)
// craft a wire minigame? + building
// casting gets u a electrical motor hull (lot of metal)
// electrical motor = hull + wires (a lot of wires)  coil winding minigame, teeb hiirega ringe
//
// craft electronic parts + building + minigame
// electric drill
// pcb (silica + raw wire)
// 
// mine building and coal
// 
// mountain goat? tegevusi oleks vaja küll juurde
//
// swampis saab puid koguda aga raskem on 0.5
// 
// 3 food (beach:fish, mountain:milk, plain:vegetables + meat)
// 4 metal (volcano: mountain: valley: limestone crater: magnetite/iron)
// different cooldowns on wood (plain: 0.5, volcano: 0.4, crater: 0.1, forest: 1, swamp: 0.4, beach: 0.2, mountain: 0.2, valley: 0.2)

//done % igal crafting asjal, stone knife on pask ja annab vähe%, obisidan on parem ja iron veel parem

// centrifuge = gears + wood
// centrifuge action name: crank
// centrifuge action name: mode = mix or centrifuge?
// upgrade centrifuge: metal construction (gears + aluminum pan?)
// upgrade centrifuge: electric motor (adds switch to turn on and off)
// centrifuge output slot: can choose vial
// centrifuge bowl slot: bucket, urn, metal bowl
// some kind of meter to show how much is done

// campfire = put stones around pile of wood
// put bowl/urn on campfire: cook/boil water

// tools equiping, into the bench? are tools resources?
// can tools be traded? maybe not, everybody needs to make own tools
// tools are tied to workbench slots and have their condition/durability?
// mining tools? mine have slot perhaps? thats why u cant use dig() forever, no tools there

// hole = dig
// case plain, forest(slow growth rate):
// farmland = hole.placeSeed;
// case biome.valley, plain, forest, beach(cant build well), swamp(cant build well)
// holeWithWater = dig()
// well = holeWithWater.buildWell()
// case biome.crater,
// bigHole = dig()
// miningQuarry = bigHole.buildQuarry() or dig()
// case biome.mountain, volcano
// miningCave = hole.dig();
// mine: there are no more loose rocks, you need more powerful tool

// alchemy workshop
// mix
// 
// build centrifuge
// 
// magnesium can be used in bricks for better heat resitant
// acid can be made of ammonia which can be extracted by heat from (Alkali salts)
// saltpeter is extracted 
// nitrate salts -> saltpeter and ammonia and nitric acid

/*

Recipies:
Chloric acid(HC) = Blue rock (slag/ash) + water
Fluoric acid(HF) = Rainbow rock (slag/ash) + water

Silicon = Sand + Mg + Heat then HC then HF
LED = Silicon
Laser = Al + White rock + LED
Magnets = Black rock + Co
Batteries = Li = (Pink)
Explosives = N = (Yellow)
Tools = Fe = (Brown 0.4, Black 0.8)
Electronics = Cu = (Green 0.4, Red 0.8)



glass = sand(0.7) + soda ash(0.2) + lime(0.1)
salt = campfire.boil(water) 0.03
salt = kiln.burn(any rock/ore) 0.01
lithium = centrifuge(salt + quicklime) 0.01
magnesium = centrifuge(salt + quicklime) 0.01
quicklime = kiln.burn(limestone) 0.2
two usage sfor quicklime
 bastingPowder = 
 concrete();


blastingPowder = sulfur + charcoal + saltpeter
Rare metals (White rock)



# Strong early-game biomes
## Plain
Can survive on own easily
Clay(0.0-0.1, Si)

## Forest
Can survive on own easily
Lime(Ca) by burning (Gray rock)
Nitrate of potash or saltpeter (K, N) (Orange crystal)

# Strong mid-game biomes
## Swamp
Can survive on own
Produces tools
Bog iron(Fe) (Brown rock)

## Beach
Can survive on own
Sand(Si)
Dioptase(Cu,Si) (Green rock)

## Desert valley (Building material)
Can't survive on own
Lime(Ca) by burning (Gray rock)
Cuprite(Cu) (Red rock)

# Strong late-game 
## Mountain
Can't survive on own
Produces chemicals
no water
Dolomite(Ca, Mg, Co) (Purple rock)
Magnetite(Fe) (Black rock)
Al, Cl (Blue rock)

## Volcano
Can't survive on own
Produces explosives and acid
Needs: water, food
Sulfur (Yellow rock)
Fluorite (Rainbow rock)


# Strong in all ages (should be played by a leader)
## Crater lake
Can survive on own
Produces batteries and rare metals
Needs: pump, eplosives, food in late-game
Clay(0.5-1)
Petalite(Li, Al) (Pink rock)
Rare metals (White rock) (needs water pump to empty the lake)




Early-game resources
Wood, water, food, horses
Goal: Survive, set up trading routes

Mid-game resources
Colored rocks, charcoal, tools
Goal: Figure out what colored rocks produce, improve trading routes, make tasks clear

Late-game resources
Chemicals, electronics and electricity
Goal: Build router and power it, improve resource handling

End-game resources
???
Goal: Automate everything?


Preferred mode:
I love animals and farming {plain, forest}
I love fishing in Runescape {beach, crater}
I want solo game {crater}
I want hardcore survival {desert, mountain, volcano}

*/

export const gatherResource = (action) => {
    const biome = store.getState().general.biome;
    let actions;
    if(biomeResourceMap.hasOwnProperty(biome))
        actions = biomeResourceMap[biome];
    else
        actions = biomeResourceMap[biomes.default];

    let availableResources = [];
    if(actions.hasOwnProperty(action)) {
        availableResources = Object.entries(actions[action]);
    } else {
        availableResources = Object.entries(biomeResourceMap[biomes.default][action]);
    }

    availableResources.forEach(([resource, meta]) => {
        if(1-Math.random() <= meta.chance) {
            let min=1, max=1, precision=1, amount = 0;
            if(typeof meta.amount === "number")
                amount = meta.amount;
            else {
                if(meta.amount instanceof Array) {
                    [min, max, precision] = meta.amount;
                }
                amount = getRandomPrecision(min, max, precision);
            }
            if(amount > 0) {
                console.log(`You got ${amount} ${resource}`);
                store.dispatch(increaseResource(resource, amount));
            }
        }
    });
}

let Game = ({biome, playerName, theme, resources, buildings, actions}) => {
    const changeName = () => {
        let newName = prompt("Please enter your name", playerName);
        if (newName != null || newName !== "") {
            actions.setPlayerName(newName);
        }
    }

    useEffect(() => {
        for(let i in theme){
            document.body.style[i] = theme[i];
        }
    }, [theme])

    const resetGame = () => {
        if(confirm("Are you sure? You will lose all the progress and resources when moving.")) {
            localStorage.removeItem("resources");
            localStorage.removeItem("buildings");
            localStorage.removeItem("counters");
            window.location.reload();
        }
    }

    //const chopWood = actions.increaseResource.bind(null, "wood", 1);
    const digHandler = () => {
        gatherResource("dig");
        actions.increaseCounter("holeDepth", 1);
    }
    const chopWood = gatherResource.bind(null, "chopWood");
    const searchBushes = gatherResource.bind(null, "searchBushes");
    
    const mapStateToProps2 = (store /*, ownProps*/) => {
        return {
            playerName: store.general.playerName,
            biome: store.general.biome,
            buildings: store.buildings,
            resources: store.resources,
        };
    }
    const Land = withBuilding(connect(mapStateToProps2)(() => <>
        <h2>Land</h2>
        <ASCII.Pre/>
        <TimedBtn text="Dig" icon={['fad', 'shovel']} time={0.5} onCooldown={digHandler}/>
        <TimedBtn text="Search bushes" icon={['fad', 'tree-alt']} time={2} onCooldown={searchBushes}/>
        <TimedBtn text="Gather wood" icon={['fad', 'trees']} time={3.8} onCooldown={chopWood}/>
    </>));

    return <>
        <h1>{playerName}, you are at the {biome}</h1>
        <button onClick={changeName}>Change name</button>
        <button onClick={actions.toggleTheme}>Toggle theme</button>
        <button onClick={resetGame}>Move to a new location</button>
        <div style={{display: "flex"}}>
            <Resources />
            <Land />
            <Workbench />
            <Hole />
            <Charcoal />
        </div>
    </>;
}
const mapStateToProps = (store /*, ownProps*/) => {
    return {
        playerName: store.general.playerName,
        biome: store.general.biome,
        theme: store.general.theme
        //buildings: store.buildings,
        //resources: store.resources,
    };
}
const mapDispatchToProps = mapDispatcher({
    increaseResource,
    increaseCounter,
    enableBuilding,
    setPlayerName,
    toggleTheme,
});
Game = connect(mapStateToProps, mapDispatchToProps)(Game);

//anvil, furnace, crusible
// tier 1 wooden - workbench
// tier 2 metal - anvil + furnace
// tier 3 powertools - workshop


const container = document.createElement("div");
document.body.append(container);
ReactDOM.render(<div>
    <Provider store={store}>
        <Game />
    </Provider>
</div>, container);
